-- CreateTable
CREATE TABLE "AvailableUsers" (
    "id" SERIAL NOT NULL,
    "user_id" INTEGER NOT NULL,

    CONSTRAINT "AvailableUsers_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "AvailableUsers" ADD CONSTRAINT "AvailableUsers_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
