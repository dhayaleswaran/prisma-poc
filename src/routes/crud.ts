import express from "express";
import { PrismaClient } from "@prisma/client";

const CrudAPIRouter = express.Router();
const prisma = new PrismaClient({
    log: ["query", "info"],
});

CrudAPIRouter.post(
    "/create",
    async (req: express.Request, res: express.Response) => {
        const createdRecord = await prisma.user.create({ data: req.body });
        return res.status(200).json(createdRecord);
    }
);

CrudAPIRouter.get(
    "/findAll",
    async (req: express.Request, res: express.Response) => {
        const all = await prisma.user.findMany({
            select: { name: true, AvailableUsers: { select: { user: true } } },
        });
        return res.status(200).json(all);
    }
);

CrudAPIRouter.get(
    "/find",
    async (req: express.Request, res: express.Response) => {
        const findOne = await prisma.user.findFirst({
            where: {
                id: Number(req.query.id),
            },
            include: {
                AvailableUsers: true,
            },
        });
        return res.status(200).json(findOne);
    }
);

CrudAPIRouter.delete(
    "/delete",
    async (req: express.Request, res: express.Response) => {
        const deleteObjs = await prisma.user.delete({
            where: { id: Number(req.query.id) },
        });
        return res.status(200).json(deleteObjs);
    }
);

CrudAPIRouter.post(
    "/relation/create",
    async (req: express.Request, res: express.Response) => {
        const relInsert = await prisma.availableUsers.create({
            data: { user_id: req.body.userId },
        });
        return res.status(200).json(relInsert);
    }
);

export default CrudAPIRouter;
