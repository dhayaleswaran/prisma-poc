import express from "express";
import CrudAPIRouter from "./routes/crud";

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/crud/", CrudAPIRouter);

export default app;
